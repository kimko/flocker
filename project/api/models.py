# project/api/models.py

import os

from flask_admin.contrib.sqla import ModelView
from sqlalchemy.sql import func

from project import db


class User(db.Model):

    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(128), nullable=False)
    email = db.Column(db.String(128), nullable=False)
    active = db.Column(db.Boolean(), default=True, nullable=False)
    created_date = db.Column(db.DateTime, default=func.now(), nullable=False)

    def to_json(self):
        return {
            "id": self.id,
            "username": self.username,
            "email": self.email,
            "active": self.active,
        }


class Message(db.Model):

    __tablename__ = "messages"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_date = db.Column(db.DateTime, default=func.now(), nullable=True)
    CreatedByNetworkId = db.Column(db.String(128), nullable=True)
    CallbackName = db.Column(db.String(128), nullable=True)
    CallbackNumber = db.Column(db.String(128), nullable=True)
    CallbackRequired = db.Column(db.String(128), nullable=True)
    CallNow = db.Column(db.String(128), nullable=True)
    PatientName = db.Column(db.String(128), nullable=True)
    PatientMrn = db.Column(db.String(128), nullable=True)
    DateOfBirth = db.Column(db.String(128), nullable=True)
    EpicInstance = db.Column(db.String(128), nullable=True)
    PatientRoom = db.Column(db.String(128), nullable=True)
    PatientBed = db.Column(db.String(128), nullable=True)
    Location = db.Column(db.String(128), nullable=True)
    Unit = db.Column(db.String(128), nullable=True)
    ServiceLine = db.Column(db.String(128), nullable=True)
    Problem = db.Column(db.String(128), nullable=False)
    Priority = db.Column(db.String(128), nullable=True)
    AssignedTo = db.Column(db.String(128), nullable=True)
    ShortDescription = db.Column(db.String(128), nullable=True)
    TicketType = db.Column(db.String(128), nullable=True)

    def to_json(self):
        return {
            "id": self.id,
            "CreatedByNetworkId": self.CreatedByNetworkId,
            "CallbackName": self.CallbackName,
            "CallbackNumber": self.CallbackNumber,
            "CallbackRequired": self.CallbackRequired,
            "CallNow": self.CallNow,
            "PatientName": self.PatientName,
            "PatientMrn": self.PatientMrn,
            "DateOfBirth": self.DateOfBirth,
            "EpicInstance": self.EpicInstance,
            "PatientRoom": self.PatientRoom,
            "PatientBed": self.PatientBed,
            "Location": self.Location,
            "Unit": self.Unit,
            "ServiceLine": self.ServiceLine,
            "Problem": self.Problem,
            "Priority": self.Priority,
            "AssignedTo": self.AssignedTo,
            "ShortDescription": self.ShortDescription,
            "TicketType": self.TicketType,
        }


if os.getenv("FLASK_ENV") == "development":
    from project import admin

    admin.add_view(ModelView(User, db.session))
    admin.add_view(ModelView(Message, db.session))
