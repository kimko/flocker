# project/api/messages.py


from flask import Blueprint, request
from flask_restful import Api, Resource

from project import db
from project.api.models import Message

messages_blueprint = Blueprint("messages", __name__)
api = Api(messages_blueprint)


class MessagesList(Resource):
    def get(self):
        response_object = {
            "status": "success",
            "data": {
                "messages": [message.to_json() for message in Message.query.all()]
            },
        }
        return response_object, 200

    def post(self):
        post_data = request.get_json()
        response_object = {"status": "fail", "message": "Invalid payload."}
        if not post_data:
            return response_object, 400
        try:
            db.session.add(Message(**post_data))
            db.session.commit()
            response_object = {
                "status": "success",
                "message": f"{post_data['Problem']} was added!",
            }
            return response_object, 201
        except TypeError:
            db.session.rollback()
            return response_object, 400


api.add_resource(MessagesList, "/messages")
