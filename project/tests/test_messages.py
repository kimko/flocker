# project/tests/test_messages.py


import json

from project.tests.utils import add_message, recreate_db


def test_add_message(test_app, test_database):
    client = test_app.test_client()
    resp = client.post(
        "/messages",
        data=json.dumps({"Problem": "bla"}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 201
    assert "bla was added!" in data["message"]
    assert "success" in data["status"]


def test_add_message_invalid_json(test_app, test_database):
    client = test_app.test_client()
    resp = client.post(
        "/messages", data=json.dumps({}), content_type="application/json"
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 400
    assert "Invalid payload." in data["message"]
    assert "fail" in data["status"]


def test_add_message_invalid_json_keys(test_app, test_database):
    client = test_app.test_client()
    resp = client.post(
        "/messages",
        data=json.dumps({"invalid": "boom"}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 400
    assert "Invalid payload." in data["message"]
    assert "fail" in data["status"]


def test_all_messages(test_app, test_database):
    recreate_db()
    add_message(Problem="plunk")
    add_message(Problem="plonk")
    client = test_app.test_client()
    resp = client.get("/messages")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert len(data["data"]["messages"]) == 2
    assert "plunk" in data["data"]["messages"][0]["Problem"]
    assert "plonk" in data["data"]["messages"][1]["Problem"]
    assert "success" in data["status"]
