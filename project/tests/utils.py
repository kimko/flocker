# project/tests/utils.py


from project import db
from project.api.models import Message, User


def add_user(username, email):
    user = User(username=username, email=email)
    db.session.add(user)
    db.session.commit()
    return user


def add_message(Problem):
    message = Message(Problem=Problem)
    db.session.add(message)
    db.session.commit()
    return message


def recreate_db():
    db.session.remove()
    db.drop_all()
    db.create_all()
