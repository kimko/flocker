build:
	# Build image locally.
	docker-compose up -d --build
	docker-compose exec users python manage.py recreate_db
	docker-compose exec users python manage.py seed_db


run:
	docker-compose up -d

test:
	# Run unit tests.
	docker-compose exec users pytest "project/tests" -p no:warnings --cov="project" --cov-report html
	open htmlcov/index.html
	docker-compose exec users flake8 project
	docker-compose exec users black project --check
	docker-compose exec users /bin/sh -c "isort project/*/*.py --check-only"

clean:
	# Remove local images.
	docker-compose down --remove-orphans
	docker stop flocker_users_1 || true
	docker stop flocker_users-db_1 || true
	docker rm flocker_users_1 || true
	docker rm flocker_users-db_1 || true

db:
	docker-compose exec users-db psql -U postgres

logs:
	docker logs -f flocker_users_1
